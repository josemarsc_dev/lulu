import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheBoloPage } from './detalhe-bolo.page';

describe('DetalheBoloPage', () => {
  let component: DetalheBoloPage;
  let fixture: ComponentFixture<DetalheBoloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalheBoloPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheBoloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
