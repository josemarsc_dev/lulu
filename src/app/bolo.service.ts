import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bolo } from './home/bolo';

@Injectable({
  providedIn: 'root'
})
export class BoloService {

  carrinho: Bolo[] = [];

  constructor(private http: HttpClient) { }

  getBolos() {
    return this.http.get('./assets/bolos.json');
  }

  getCarrinho(): Bolo[] {
    return this.carrinho;
  }

  adcionarCarrinho(bolo) {
    this.carrinho.push(bolo);
  }

  removerCarrinho(id) {
    for (let i = 0; i < this.carrinho.length; i++) {
      if (this.carrinho[i].id == id) {
        for (let j = i; j < this.carrinho.length; i++) {
          this.carrinho[j] = this.carrinho[j + 1];
        }
        break;
      }
    }
  }
}
