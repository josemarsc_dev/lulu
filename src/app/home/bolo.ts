import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Bolo {
  id: number;
  nome: string;
  descricao: string;
  imagem: string;
  preco: number;
}
